;;;; This file is part of Gamma.                           -*- scheme -*-
;;;; Copyright (C) 2018 Sergey Poznyakoff
;;;; 
;;;; Gamma is free software; you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation; either version 3, or (at your option)
;;;; any later version.
;;;;
;;;; Gamma is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Public License
;;;; along with Gamma.  If not, see <http://www.gnu.org/licenses/>.

changequote([,])dnl
(define-module (gamma loader))

(define-public (gamma-module-load name init)
  (call/cc
   (lambda (return)
     (do ((lib-path (parse-path (getenv "LD_LIBRARY_PATH") (list "LIBDIR"))
		    (cdr lib-path)))
	 ((null? lib-path))
       (let ((dir (car lib-path)))
	 (do ((suffixes '("-v-VERSION" "") (cdr suffixes)))
	     ((null? suffixes))
	   (let ((suf (car suffixes)))
	     (catch 'misc-error
		    (lambda ()
		      (load-extension
		       (string-append dir "/" "libgamma-" name suf) init)
		      (return))
		    (lambda (key . args)
		      #t))))))
     (error "cannot load extension " name))))

