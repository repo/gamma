@c This file is part of the Gamma manual.
@c Copyright (C) 2010 Sergey Poznyakoff
@c See gamma.texi for copying conditions.
@c *******************************************************************
@cindex syslog
The @samp{(gamma syslog)} module provides bindings for @samp{syslog}
functions:

@lisp
(use-modules ((gamma syslog)))
@end lisp

@deffn {Scheme procedure} openlog tag option facility
Opens a connection to the system logger for Guile program.
Arguments have the same meaning as in openlog(3):

@table @var
@item tag
Syslog @dfn{tag}: a string that will be prepended to every message.

@item option
Flags that control the operation.  A logical or (@code{logior}) of
one or more of the following:

@table @asis
@kwindex LOG_CONS
@item LOG_CONS
Write directly to system console if there is an error while sending to
system logger.

@kwindex LOG_NDELAY
@item LOG_NDELAY
Open the connection immediately (normally, the opening is delayed
until when the first message is logged).

@kwindex LOG_NOWAIT
@item LOG_NOWAIT
Don't wait for child processes that may have been created while
logging the message.

@kwindex LOG_ODELAY
@item LOG_ODELAY
The converse of @samp{LOG_NDELAY}; opening of the connection is
delayed until @code{syslog} is called.  This is the default.

@kwindex LOG_PERROR
@item LOG_PERROR
Print to stderr as well.  This constant may be absent if the
underlying implementation does not support it.

@kwindex LOG_PID
@item LOG_PID
Include PID with each message.
@end table

@cindex facility, syslog
@cindex syslog facility
@item facility
Specifies what type of program is logging  the  message.
The facility must be one of:

@multitable @columnfractions 0.3 0.7
@headitem Facility @tab Meaning
@kwindex LOG_AUTH
@item LOG_AUTH      @tab Security/authorization messages.
@kwindex LOG_AUTHPRIV
@item LOG_AUTHPRIV  @tab Same as @code{LOG_AUTH}.
@kwindex LOG_CRON
@item LOG_CRON      @tab Clock daemon.
@kwindex LOG_DAEMON
@item LOG_DAEMON    @tab System daemons without separate facility value.
@kwindex LOG_FTP
@item LOG_FTP       @tab @acronym{FTP} daemon.
@kwindex LOG_LOCAL0
@kwindex LOG_LOCAL1
@kwindex LOG_LOCAL2
@kwindex LOG_LOCAL3
@kwindex LOG_LOCAL4
@kwindex LOG_LOCAL5
@kwindex LOG_LOCAL6
@kwindex LOG_LOCAL7
@item LOG_LOCAL0 through LOG_LOCAL7 @tab Reserved for local use.
@kwindex LOG_LPR
@item LOG_LPR       @tab Line printer subsystem.
@kwindex LOG_MAIL
@item LOG_MAIL      @tab Mail subsystem.
@kwindex LOG_NEWS
@item LOG_NEWS      @tab @acronym{USENET} news subsystem.
@kwindex LOG_SYSLOG
@item LOG_SYSLOG    @tab Messages generated internally by @command{syslogd}.
@kwindex LOG_USER
@item LOG_USER      @tab Generic user-level messages.  This is the default.
@kwindex LOG_UUCP
@item LOG_UUCP      @tab @acronym{UUCP} subsystem.
@end multitable

Example:

@lisp
(openlog "reader" (logior LOG_PID LOG_CONS) LOG_DAEMON)
@end lisp
@end table
@end deffn

@deffn {Scheme procedure} syslog-tag
Returns the tag, used in the recent call to @code{openlog}.
@end deffn

@deffn {Scheme procedure} syslog prio text
Distribute a message via @command{syslogd}.  The @var{text} supplies
the message text.  The @var{prio} specifies @dfn{priority} of the
message.  Its value must be one of the following:

@cindex priority, syslog
@cindex syslog priority
@multitable @columnfractions 0.3 0.7
@headitem Priority @tab Meaning
@kwindex LOG_EMERG
@item LOG_EMERG    @tab system is unusable
@kwindex LOG_ALERT
@item LOG_ALERT    @tab action must be taken immediately
@kwindex LOG_CRIT
@item LOG_CRIT     @tab critical conditions
@kwindex LOG_ERR
@item LOG_ERR      @tab error conditions
@kwindex LOG_WARNING
@item LOG_WARNING  @tab warning conditions
@kwindex LOG_NOTICE
@item LOG_NOTICE   @tab normal, but significant, condition
@kwindex LOG_INFO
@item LOG_INFO     @tab informational message
@kwindex LOG_DEBUG
@item LOG_DEBUG    @tab debug-level message
@end multitable

Example:

@lisp
(syslog LOG_WARNING "This is a test message")
@end lisp

The priority argument may also be @samp{OR}ed with a facility value, to
override the one set by the @code{openlog} function, e.g.:

@lisp
(syslog (logior LOG_DAEMON LOG_WARNING) "This is a test message")
@end lisp

It is common to use the @code{format} function to prepare the value
of the @var{text} argument:

@lisp
@group
(syslog LOG_WARNING
   (format #f "operation reported: ~A" result))
@end group
@end lisp
@end deffn

@deffn {Scheme procedure} open-syslog-port prio
Create a @dfn{syslog port} for the given priority.  Syslog port is a
special output port such that any writes to it are transferred to
the syslog with the given priority.  The port is line buffered.  For
example, the following code:

@lisp
@group
(set-current-output-port (open-syslog-port LOG_ERR))
(display "A test ")
(display "message")
(newline)
@end group
@end lisp

@noindent
results in sending the string @samp{A test message} to the syslog
priority @code{LOG_ERR}.
@end deffn


@deffn {Scheme procedure} openlog?
Return @code{#t} if @code{openlog} was previously called.
@end deffn

@deffn {Scheme procedure} closelog
Close the logging channel.  The use of this function is optional.
@end deffn






