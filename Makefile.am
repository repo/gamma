# This file is part of Gamma.
# Copyright (C) 2002-2018 Sergey Poznyakoff
#
# Gamma is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# Gamma is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Gamma.  If not, see <http://www.gnu.org/licenses/>.

ACLOCAL_AMFLAGS = -I m4 -I am -I gint

SUBDIRS = gint gamma scripts examples doc

# Define the following variables in order to use the ChangeLog rule below:
#  prev_change_log  [optional]  Name of the previous ChangeLog file.
#  gen_start_date   [optional]  Start ChangeLog from this date. 
#  changelog_dir    [mandatory] Directory where to create ChangeLog
gen_start_date = 2008-06-21
prev_change_log = ChangeLog.cvs
changelog_dir = .

.PHONY: ChangeLog
ChangeLog: 
	$(AM_V_GEN)if test -d .git; then                                   \
	  cmd=$(top_srcdir)/scripts/gitlog-to-changelog;                   \
	  if test -n "$(gen_start_date)"; then                             \
	    cmd="$$cmd --since=\"$(gen_start_date)\"";                     \
	  fi;                                                              \
	  $$cmd --format='%s%n%n%b%n' |                                    \
            sed '/<unknown>$$/d' | fmt -s > $(changelog_dir)/cl-t;         \
          if test -n "$(prev_change_log)" && test -f "$(prev_change_log)"; \
	  then                                                             \
	    echo "" >> $(changelog_dir)/cl-t;                              \
	    cat "$(prev_change_log)" |                                     \
	      sed '/^Local Variables:/,/^End:/d' >> $(changelog_dir)/cl-t; \
	  fi;                                                              \
	  echo "Local Variables:" >> $(changelog_dir)/cl-t;                \
	  echo "mode: change-log" >> $(changelog_dir)/cl-t;                \
	  echo "version-control: never"  >> $(changelog_dir)/cl-t;         \
	  echo "buffer-read-only: t" >> $(changelog_dir)/cl-t;             \
	  echo "End:" >> $(changelog_dir)/cl-t;                            \
	  rm -f $(changelog_dir)/ChangeLog;                                \
	  mv $(changelog_dir)/cl-t $(changelog_dir)/ChangeLog;             \
	fi

dist-hook:
	@here=`pwd`; \
	modules=`cd $(top_srcdir)/modules; pwd`; \
	distdir=`cd $(distdir) && pwd`; \
	mkdir $(distdir)/modules; \
	cd $$modules; \
	grep -v '#' MODLIST | tr -s '\n' | \
	 while read name; do \
	   cp $$name $$distdir/modules; \
         done; \
        cp MODLIST $$distdir/modules; \
	cd $$here

#Makefile.am: $(top_srcdir)/modules/MODLIST
#	scripts/bootstrap -C $(top_srcdir) --moddir modules --parents
